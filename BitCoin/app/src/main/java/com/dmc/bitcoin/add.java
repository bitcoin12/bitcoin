package com.dmc.bitcoin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class add extends AppCompatActivity {
    EditText code,currency,rate,discription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        code = findViewById(R.id.code);
        currency = findViewById(R.id.currency);
        rate = findViewById(R.id.rate);
        discription = findViewById(R.id.discription);
    }
    public void add(View view){
        bitcoindata data = new bitcoindata(Integer.parseInt(code.getText().toString()),
                currency.getText().toString(),Integer.parseInt(rate.getText().toString()),discription.getText().toString());


        Intent intent = new Intent();
        intent.putExtra("k_country",data);
        setResult(0,intent);
        finish();

    }

}